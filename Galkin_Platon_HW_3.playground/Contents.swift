import UIKit



func makeBuffer() -> (String) -> Void {
    var listOfWords = [String]()
    
    func buffer(str: String) {
        if str.count > 0 {
            listOfWords.append(str)
        } else {
            print(listOfWords.joined(separator:""))
        }
    }
    return buffer
}


var buffer = makeBuffer()
buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("") // Замыкание использовать нужно!
