import UIKit



// Задание 1
let milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

// Задание 2 + изменение из 3-го
var milkPrice: Double = 3

// Задание 3
milkPrice = 4.20

// Задание 4
var milkBottleCount: Int? = 20
var profit: Double = 0.0
if let Bottle = milkBottleCount {
    profit = milkPrice * Double(Bottle)
}
print(profit)

// Принудительное развертывание не стоит использовать, т.к. может отказаться, что в момент развертывания в переменной храниться nil. В следствии такого развертывания получим ошибку. Безопасное развертывание можно выполнять несколькими способами помимо if let описанного в курсе. Так же можно использовать оператор Guard. Так же есть Optional chaining, Nil coalescing operator и Optional pattern. Пример: возьмем автомобиль и топливный бак. Бак имеет какой-то объем и на приборной панели отображается текущий уровень топлива. Бак может быть полный, а может оказаться пустым. Если делать небезопасное извлечение, то в момент, когда в баке нет бензина приборная панель "сломается". Возможно не самый лучший пример, учитывая что может быть просто 0, но если принять локику что если нет бензина - нет значения, то все работает

// Задание 5
var employeesList = [String]()
employeesList += ["Иван", "Петр", "Геннадий", "Андрей", "Марфа"]

//Задание 6
var isEveryoneWorkHard = false
var workingHours = 39
if workingHours >= 40 {
    isEveryoneWorkHard = true
}
print(isEveryoneWorkHard)
