import UIKit


func checkPrimeNumber(number: Int) -> Bool {
    guard number >= 2 else {return false}
    
    for i in 2 ..< number {
        if number % i == 0 {
            return false
        }
    }
    return true
}

checkPrimeNumber(number: 7)
checkPrimeNumber(number: 8)
checkPrimeNumber(number: 13)
